# <u>EXERCICIS XML </u>
## Pràctiques

+ __Amb una còpia del catàleg de CDs o del fitxer /etc/fonts/fonts.conf (en la còpia pots eliminar la declaració de tipus de document) provoca deliberadament tots els errors estudiats (verifica els documents amb <u>xmlwf o xmllint --noout).__</u>

1. Comprovem que el fitxer original està ben format:
```
[isx46410800@i05 aux]$ xmllint cd_catalog.xml --noout
[isx46410800@i05 aux]$
```

2. Fem una còpia, eliminem coses i tornem a fer la comprovació:
```
[isx46410800@i05 aux]$ xmllint cd_catalog_mal.xml --noout
cd_catalog_mal.xml:9: parser error : Opening and ending tag mismatch: COMPANY line 6 and CD
	</CD>
	     ^
cd_catalog_mal.xml:17: parser error : Opening and ending tag mismatch: ARTIST line 12 and CD
	</CD>
	     ^
cd_catalog_mal.xml:210: parser error : Opening and ending tag mismatch: COMPANY line 6 and CATALOG
</CATALOG>
          ^
cd_catalog_mal.xml:211: parser error : Premature end of data in tag TITLE line 3
^
cd_catalog_mal.xml:211: parser error : Premature end of data in tag CD line 2
^
cd_catalog_mal.xml:211: parser error : Premature end of data in tag CATALOG line 1
```

+ __Identifica les parts de l’XML Syntax Quick Reference que tracten dels documents ben formats.__

    - Element Declaration
    - Predefined General Entities
    - Empty Element
    - CDATA Section
    - Comment


+ __Practica els tutorials citats en la llista d’enllaços: per exemple,crea localment tots els documents presentats, verifica el seu contingut, etc.__
	- Comprovem el contingut de si està ben format:
	```
	[isx46410800@miguel-fedora27 UF1]$ xmllint fonts.xml --noout
	[isx46410800@miguel-fedora27 UF1]$ xmllint example1.xml --noout
	[isx46410800@miguel-fedora27 UF1]$ xmllint example2.xml --noout
	[isx46410800@miguel-fedora27 UF1]$
	```

	- Fem algun canvi per a que doni errors:
	```
	[isx46410800@miguel-fedora27 UF1]$ xmllint example1_canvis.xml --noout
	example1_canvis.xml:1: parser error : parsing XML declaration: '?>' expected
	<?xml version="1.0" encoding="UTF-8"?
                                    ^
	example1_canvis.xml:2: parser error : Start tag expected, '<' not found
  Tove</to>
  ^
	```

+ __Usa l’ordre xmllint(1) (opció --format) per formatar correctament un document XML (prèviament desformatat deliberadament).__

1. Prèviament fem canvis deliberadament del format del document.

```
<CATALOG>
	<CD>
		<TITLE>Empire Burlesque</TITLE>
			<ARTIST>Bob Dylan</ARTIST>
				<COUNTRY>USA</COUNTRY>
					<COMPANY>Columbia</COMPANY>
						<PRICE>10.90</PRICE>
		<YEAR>1985</YEAR>
	</CD>
<CD>
		<TITLE>Hide your heart</TITLE>
		<ARTIST>Bonnie Tyler</ARTIST>
		<COUNTRY>UK</COUNTRY>
		<COMPANY>CBS Records</COMPANY>
		<PRICE>9.90</PRICE>
		<YEAR>1988</YEAR>
</CD>
```

2. Utilitzem l'ordre:

`[isx46410800@i05 aux]$ xmllint cd_catalog_format.xml --format | head -n20`

```
<?xml version="1.0"?>
<CATALOG>
  <CD>
    <TITLE>Empire Burlesque</TITLE>
    <ARTIST>Bob Dylan</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Hide your heart</TITLE>
    <ARTIST>Bonnie Tyler</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS Records</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
  <CD>
    <TITLE>Greatest Hits</TITLE>
```
> En la sortida per STDOUT queda tot identat el fitxer xml.
