# <u>EXERCICIS CODIFICACIÓ</u>

#### 1. Consulta les pàgines de manual ascii(7), latin1(7), utf8(7).
+ __man ascii__

```
NAME
       ascii - ASCII character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       ASCII  is  the American Standard Code for Information Interchange.  It is a 7-bit code.
       Many 8-bit codes (e.g., ISO 8859-1) contain ASCII as their lower  half.   The  interna‐
       tional counterpart of ASCII is known as ISO 646-IRV.

       The following table contains the 128 ASCII characters.
```

+ __man latin1__

```
NAME
       iso_8859-1 - ISO 8859-1 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO  8859  standard  includes  several 8-bit extensions to the ASCII character set
       (also known as ISO 646-IRV).  ISO 8859-1 encodes the characters used in many West Euro‐
       pean languages.

```

+ __man 7 utf8__

```
NAME
       UTF-8 - an ASCII compatible multibyte Unicode encoding

DESCRIPTION
       The  Unicode  3.0 character set occupies a 16-bit code space.  The most obvious Unicode
       encoding (known as UCS-2) consists of a sequence of 16-bit  words.   
```
> La codificació _ASCII_ té 7 bits iguals per a tot el món. _Latin1_ afegeix un bit més que és diferent per a cada país, es a dir, cada país té 128 chars més del ASCII diferents de cada llenguatge Per últim _UTF8_ conté tots els altres i té en total 2 bytes de codificació de caràcters.

#### 2. Compara el resultat de les ordres man passwd i man 5 passwd.

+ __man passwd__

```
NAME
       passwd - update user's authentication tokens

DESCRIPTION
       The passwd utility is used to update user's authentication token(s).
```       


+ __man 5 passwd__

```
NAME
       passwd - password file

DESCRIPTION
       The  /etc/passwd file is a text file that describes user login accounts for the system.
       It should have read permission allowed for all users (many utilities, like ls(1) use it
       to map user IDs to usernames), but write access only for the superuser.
```

> La diferència que hi ha és que en el primer fa referència a l'ordre per a canviar-se la contrasenya mentre que la segona es refereix al fitxer _/etc/passwd_ on es descriu els comptes d'usuaris del sistema.

#### 3. Verifica que entens tots els exemples anteriors de redirecció del shell.

Exemple:
```
iconv -f ISO88592 -t UTF8 < input.txt > output.txt
```
__-f:__ ens indica des de quina codificació partim.  
__-t:__ ens indica a quina codificació volem fer.  
__<:__ ens indica la redirecció d'entrada.  
__\>:__ ens indica la redirecció de sortida.

#### 4. Converteix un fitxer existent en el sistema a finals de línia propis de Windows.

__1. Creem un fitxer del sistema.__

```
[isx46410800@miguel-fedora27 UF1]$ vim salto_linea.txt

[isx46410800@miguel-fedora27 UF1]$ file salto_linea.txt
salto_linea.txt: UTF-8 Unicode text

[isx46410800@miguel-fedora27 UF1]$ cat salto_linea.txt
Esto es un fichero ejemplo de salto de línea.
Tenemos que simular un fichero con salto de final de línea propio de windows.

```

__2. Passem el fitxer a salt de línia propi de Windows__

```
[isx46410800@miguel-fedora27 UF1]$ sed 's/$/\r/' salto_linea.txt > salto_linea_windows.txt
```

__3. Comprovem el fitxer__

```
[isx46410800@miguel-fedora27 UF1]$ file salto_linea_windows.txt
salto_linea_windows.txt: UTF-8 Unicode text, with CRLF line terminators

[isx46410800@miguel-fedora27 UF1]$ cat salto_linea_windows.txt
Esto es un fichero ejemplo de salto de línea.
Tenemos que simular un fichero con salto de final de línea propio de windows.
```
