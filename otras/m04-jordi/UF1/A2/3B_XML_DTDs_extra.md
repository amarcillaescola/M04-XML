4.1. A la Pràctica2 se us demanava implementar els XML corresponents. En aquesta es demana al mateix doc XML implementar un DTD adient. Fixeu-vos que els scripts tindran el mon que tenien però amb una "b", no canvieu l'original!

[2.1.xml](./scripts/2.1b.xml)
[2.2.xml](./scripts/2.2b.xml)
[2.3.xml](./scripts/2.3b.xml)
[2.4.xml](./scripts/2.4b.xml)
[2.5.xml](./scripts/2.5b.xml)


4.2. Donat aquest XML, implementeu un dou doc amb un DTD adient:

```
<cinema>
   <pelis>
      <peli codpeli = "P1"> Avatar </ peli>
      <peli codpeli = "P2"> Mystic River </ peli>
      <peli codpeli = "P3"> The Terminator </ peli>
      <peli codpeli = "P4"> Titanic </ peli>
   </pelis>
   <directors>
      <director filmografia = "P2"> Clint Eastwood </director>
      <director filmografia = "P1 P3 P4"> James Cameron </director>
   </ directors>
</cinema>
```
