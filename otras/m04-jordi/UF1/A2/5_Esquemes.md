# Esquemes

Aquí teniu un [fitxer XML](scripts/5.0.receptes.xml) i el corresponent [esquema](scripts/5.0.receptes.xsd) que el valida. Per validar-lo tan sols heu d'executar les següents opcions de xmllint:
```
xmllint  5.0.receptes.xml --schema 5.0.receptes.xsd --noout
```

Si tot és correcte, l'anterior ordre retornarà el següent missatge:
```
5.0.receptes.xml validates
```

### Exercicis:

5.1. Tenim el següent fitxer XML [5.1.libraryAmbErrors.xml](scripts/5.1.libraryAmbErrors.xml).

<ins>Apartat A</ins>. Reviseu i corregiu errors. El nou fitxer es dirà `5.1.library.xml`.
```
<? xml version = "1.0"?>
<! DOCTYPE library
[
      <! ELEMENT library (book +)>
      <! ELEMENT book (títol, autor, ref?, genre+)>
      <! ELEMENT title (#PCDATA)>
      <! ELEMENT author (#PCDATA)>
      <! ELEMENT ref (#PCDATA)>
      <! ELEMENT genre (drama|comedia|accio|infantil)>
 ]
>
<Library>
<Llibre>
        <title> N o M </ title>
        <autor> Agatha Christie </ author>
        <Ref> detectiu-C-15 </ ref>
</ Book>
<Llibre>
        <title> El gos de Baskerville </ title>
        <autor> Sir Arthur Conan Doyle </ author>
        <Ref> detectiu-D-3 </ ref>
</ Book>
<Llibre>
        <Títol> Duna </ title>
        <autor> Franck Heckbert </ author>
        <Ref> ficció-H-1 </ ref>
</ Book>
</ Biblioteca>
```
<ins>Apartat B</ins>. Declarar un esquema vàlid (`5.1.library.xsd`) pel fitxer 5.1.library.xml

5.2. Feu una còpia del fitxer [5.0.receptes.xml](scripts/5.0.receptes.xml) amb el nom `5.0.receptesAmbErrors.xml`. Introduiu errors expressament ( per exemple canviar valors, on toca booleà posar altre tipus de dades o en la llista enumerada posar un valor no vàlid, etc). Comproveu amb xmllint els missatges d'error que dóna i explique-los al fitxer `5.2.receptes.md`.

5.3. Donat el fitxer [5.3.coleccio.xml](scripts/5.3.coleccio.xml), crea el seu esquema corresponent `5.3.coleccio.xsd`.

5.4. Donat aquest fitxer [5.4.llibres.xml](scripts/5.4.llibres.xml), crea el seu esquema corresponent `5.4.llibres.xsd`.
