# <u>PRÀCTIQUES ENTITATS</u>

+ __Prenent com a base el catàleg de CDs del tutorial d’XML, prepara un document XML que utilitzi entitats generals internes i externes (sempre declarades en el subconjunt intern del DTD).__

  + [script xml amb entitats](./scripts/cds.xml)
  + [entitat externa](./scripts/cd.txt)  


+ __En el document declara entitats generals internes per 5 caràcters que no apareguin en el teclat, com ara el €.__
  - Entitats:
  ```
  <!ENTITY euro "&#8364;">
  <!ENTITY dolar "&#36;">
  <!ENTITY copyright "&#169;">
  <!ENTITY arroba "&#64;">
  <!ENTITY omega "&#937;">
  ```

+ __En el fitxer associat a una entitat general externa posa, per exemple, el text de la descripció d’un CD.__
  - Definim l'entitat externa en el DTD:
  `<!ENTITY desc SYSTEM "./cd.txt">`

  - En un document extern, escrivim el valor literal d'aquesta entitat:  
  `<DESCRIPTION>Producido por el propio Dylan y grabado entre Nueva York y Los Ángeles a lo largo de dos años</DESCRIPTION>`


+ __Expandeix el document fent servir l’ordre xmllint(1) (opcions: --noent --encode utf8 --loaddtd).__  
  - Veiem el codi amb `xmllint cds.xml`
    ```
  [isx46410800@miguel-fedora27 scripts]$ xmllint cds.xml
  <?xml version="1.0" encoding="UTF-8"?>
  <!DOCTYPE CATALOG [
  <!ELEMENT CATALOG (CD)*>
  <!ELEMENT CD (TITLE , ARTIST , COUNTRY , COMPANY , PRICE , YEAR , DESCRIPTION?)>
  <!ELEMENT TITLE (#PCDATA)>
  <!ELEMENT ARTIST (#PCDATA)>
  <!ELEMENT COUNTRY (#PCDATA)>
  <!ELEMENT COMPANY (#PCDATA)>
  <!ELEMENT PRICE (#PCDATA)>
  <!ELEMENT YEAR (#PCDATA)>
  <!ELEMENT DESCRIPTION (#PCDATA)>
  <!ENTITY euro "&#8364;">
  <!ENTITY dolar "&#36;">
  <!ENTITY copyright "&#169;">
  <!ENTITY arroba "&#64;">
  <!ENTITY omega "&#937;">
  <!ENTITY desc SYSTEM "./cd.txt">
  ]>
  <CATALOG>
    <CD>
      <TITLE>Empire Burlesque</TITLE>
      <ARTIST>Bob Dylan</ARTIST>
      <COUNTRY>USA</COUNTRY>
      <COMPANY>&copyright;Columbia</COMPANY>
      <PRICE>10.90 &euro;</PRICE>
      <YEAR>1985</YEAR>
      &desc;
    ```

  - Expandim el document amb:  
  `xmllint cds.xml --noent --encode utf8 --loaddtd cd.txt`

    ```
    <CATALOG>
      <CD>
        <TITLE>Empire Burlesque</TITLE>
        <ARTIST>Bob Dylan</ARTIST>
        <COUNTRY>USA</COUNTRY>
        <COMPANY>©Columbia</COMPANY>
        <PRICE>10.90 €</PRICE>
        <YEAR>1985</YEAR>
        <DESCRIPTION>Producido por el propio Dylan y grabado entre Nueva York y Los Ángeles a lo largo de dos años</DESCRIPTION>

      </CD>
    ```
> Veiem com s'ha fet l'expansió de l'entitat 'desc'.
