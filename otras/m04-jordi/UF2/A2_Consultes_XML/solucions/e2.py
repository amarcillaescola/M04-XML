#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Exercici 2. 2. Llistat de tots els municipis.

from lxml import etree
arbol = etree.parse ("poblacions.xml")
localidades = arbol.xpath ("/lista/provincia/localidades/localidad")
for localidad in localidades :
    print localidad.text
