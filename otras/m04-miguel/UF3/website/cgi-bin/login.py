#!/usr/bin/python3
#Miguel Amorós
# Formulari per fer login: si la contrasenya no és correcte es còpia
# com a resposta de nou el formulari.

import sys
import cgi

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

# conseguimos los valores del login y password
user = form.getvalue("email")
password = form.getvalue("pwd")

if user == "miguel14amoros@gmail.com" and password == "12345678":
    print("Inicio de sesión correcto")
    print("<br>")
    print('')
    print('<html>')
    print('  <head><br><br>')
    print('    <a href="http://localhost:8000/UF3A1T3_login.html">Volver atrás</a>')
    print('  </head>')
    print('</html>')
else:
    print("Inicio de sesión incorrecto")
    print("<br>")
    redirectURL = "http://localhost:8000/UF3A1T3_login.html"
    print('<html>')
    print('  <head>')
    print('    <meta http-equiv="refresh" content="0;url='+str(redirectURL)+'" />')
    print('  </head>')
    print('</html>')

sys.exit(0)

# vim:sw=4:ts=4:ai:et
