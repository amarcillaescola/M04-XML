# Exercicis dataset computer.xml
    sudo dnf -y install lshw
    sudo lshw -xml fitxerSortida.xml

1. Inserir un nou disc dur marca SEAGATE de 512 GB (fixeu-vos en quines unitats s'expressa al dataset).
2. Actualitar el processador i7-6700 per un AMD Ryzen 7 2700 4.1 Ghz (utilitzeu la funció de xpath contains). Si la informació hi és a més d'un element, s'ha d'actualitzar a tot arreu.
3. Esborrar els bancs 0 i 2 de memòria (hi ha 4 bancs).
4. Canvieu la placa mare per un model diferent.
