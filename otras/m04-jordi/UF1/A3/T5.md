Aplicar fulls d’estil a documents XHTML
=======================================

MP4UF1A3T5

Disseny de la presentació amb CSS

Funció dels fulls d’estil
-------------------------

-   Els fulls d’estil permeten separar contingut i presentació
    (<http://www.csszengarden.com/>).
-   Els fulls d’estil permeten [separar responsabilitats](aux/soc.png)
    entre autor, editor i grafista.
-   Disposar de full d’estil fa que el marcat HTML pugui ser
    purament semàntic.

Estructura d’un full d’estil CSS
--------------------------------

-   Consta de 1 o més regles, i comentaris com els del llenguatge C
    (`/* ... */`).
-   Cada regla consta d’un selector i una llista de propietats.
-   Cada propietat consta d’una parella nom/valor.
-   Exemple de regla:

        p {
            font-family: "Liberation serif", "Times New Roman", serif;
            font-size: 20px;
        }

Enllaç entre HTML i CSS
-----------------------

-   Forma preferida:

        <link rel="stylesheet" href="style.css" type="text/css"/>

-   Alternativa: element HTML `style`.
-   Alternativa: atribut HTML `style`.

Selectors principals
--------------------

-   Un element: `p {…`
-   Classe (atribut HTML `class`): `.important {…`
-   Identificador únic (atribut HTML `id`): `#menu {…`
-   Selectors agrupats: `p, div {…`
-   Selectors contextuals: `em b {…`

Eines d’edició i estudi per CSS
-------------------------------

-   Consola d’errors de Firefox (`Ctrl+Shift+J`).
-   [Firebug](http://getfirebug.com/), plugin per Firefox.
-   [CSS Validation Service](http://jigsaw.w3.org/css-validator/).

Enllaços recomanats
-------------------

-   [CSS Tutorial](http://www.w3schools.com/css/)
-   [WDG CSS 1 Guide](http://htmlhelp.com/distribution/wdgcss.tar.gz)
    ([local](aux/wdgcss.tar.gz), [en
    espanyol](http://htmlhelp.com/es/reference/css/))
-   [CSS 2.1 Quick Reference
    Card](http://www.explainth.at/downloads/cssquick.pdf)
    ([local](aux/cssquick.pdf))
-   [Text llatí (Lorem Ipsum)](http://es.lipsum.com/)

**Molt important:** porta impressa a classe la CSS 2.1 Quick Reference
Card.

Pràctiques
----------

-   Instal·la a `/usr/local/doc` la guia CSS del WDG (i enllaça la
    pàgina principal en els *bookmarks* del nevegador).
-   Practica el tutorial citat en els enllaços recomanats.
-   En la guia del WDG estudia les seccions “Estructura y reglas de CSS”
    i “Enlazando Hojas de estilo con HTML”.
-   Instal·la Firebug i fes-lo servir per inspeccionar el codi de la web
    <http://es.wikipedia.org/>.
-   Crea al teu gust un full d’estil *per defecte* (et pots inspirar en
    [Sample style sheet for HTML
    2.0](http://www.w3.org/TR/REC-CSS1/#appendix-a)).
-   Utilitza aquest [reset stylesheet
    CSS](http://meyerweb.com/eric/tools/css/reset/) per anul·lar els
    estils per defecte del navegador abans de definir els teus. En els
    documents XHTML has de fer servir elements `link` separats per
    aquest full d’estil i el teu propi.
-   Utilitza aquest [normalize stylesheet
    CSS](https://github.com/necolas/normalize.css/blob/master/normalize.css)
    per corregir els estils per defecte del navegador abans de definir
    els teus. En els documents XHTML has de fer servir elements `link`
    separats per aquest full d’estil i el teu propi.
-   A sobre de `normalize.css` pots usar un mini-frameworl com
    [Milligram](https://github.com/milligram/milligram/blob/master/dist/milligram.css).
    A posar en la teva pàgina HTML una vegada hagis baixat
    `normalize.css` i `milligram.css`:

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"/>
        <link rel="stylesheet" href="normalize.css"/>
        <link rel="stylesheet" href="milligram.css"/>


