### Exercicis per fer amb python i XPath (poblacions.xml)

__1. Llistat de totes les provincies.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
for provincia in provincias:
  print provincia.text
```

__2. Llistat de tots els municipis.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
for municipi in municipis:
  print municipi.text
```
__3. Mostra la lista de provincies i el total de municipis que te cadascuna.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')

for provincia in provincias:
  count = 0
  for municipi in municipis:
    count += 1
  print provincia.text, "total:", count
```
__4. Demana per teclat el nom d'una província i mostra els seus municipis.__
__5. Demana per teclat el nom d'un municipi i mostra la provincia on es troba__
