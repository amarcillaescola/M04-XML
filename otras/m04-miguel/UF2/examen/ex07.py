#!/usr/bin/python
# -*- coding:utf-8-*-
#Miguel Amoros
#Mostrar el nom dels profes que fan classe els dimarts
import sys
from lxml import etree
root = etree.parse ('institut_examen.xml')
#//Classe[@dia="Dimarts"]/../@professor
lista_nom_profes = root.xpath("//Professor[@id =//Classe[@dia=\"Dimarts\"]/../@professor]/Nom")

#Para cada profe de los martes, printamos el nombre del profe
for nom in lista_nom_profes:
	print nom.text
	
root.write("institut_examen.xml")

sys.exit(0)

