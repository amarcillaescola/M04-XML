#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO2
## Llistat de tots els municipis
from lxml import etree
arbol = etree.parse ('poblacions.xml')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
for municipi in municipis:
  print municipi.text
