## Schemas

Aquí teniu un [fitxer XML](scripts/5.0.receptes.xml) i el corresponent [esquema](scripts/5.0.receptes.xsd) que el valida. Per validar-lo tan sols heu d'executar les següents opcions de xmllint:
```
xmllint  5.0.receptes.xml --schema 5.0.receptes.xsd --noout
```

Si tot és correcte, l'anterior ordre retornarà el següent missatge:
```
5.0.receptes.xml validates
```

### Exercicis:

5.1. Al [fitxer XML](scripts/5.0.receptes.xml) heu de canviar valors (per exemple, on toca booleà posar altre tipus de dades o en la llista enumerada posar un valor no vàlid, etc). Comproveu amb xmllint el missatge d'errors que dóna.

5.2. Donat aquest fitxer [fitxer XML](scripts/5.1.coleccio.xml), crea el seu esquema corresponent.

5.3. Donat aquest fitxer [fitxer XML](scripts/5.2.llibres.xml), crea el seu esquema corresponent.
