#!/usr/bin/python
# -*- coding: utf-8 -*-
# Convertir a majúscules els noms d’atributs.

import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

# para cada clave, asignamos el nombre del atributo en mayusculas
for element in tree.iter():
		claus = element.attrib.keys()
		for k in claus:
			#ASIGNARIAMOS LANG="EN" lang="en"
			element.attrib[k.upper()] = element.attrib[k]
			#ELIMINAMOS EL ANTIGUO
			del element.attrib[k]
	
# save
tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et
