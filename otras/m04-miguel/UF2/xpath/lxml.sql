--instalacion
dnf install -y python-lxml

--para importar libreria
from lxml import etree
doc = etree.parse('file.xml')

--convertir a cajas DOT
>>> print etree.tostring(doc,pretty_print=True ,xml_declaration=True, encoding="utf-8")
<?xml version='1.0' encoding='utf-8'?>
<biblioteca>
  <libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Pantaleón y las visitadoras</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Conversación en la catedral</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1969"/>
  </libro>
</biblioteca>

--convertir la raiz y decir su etiqueta
raiz=doc.getroot()
>>> print raiz.tag
biblioteca

--el titulo del primer libro
>>> print raiz[0][0].text
La vida está en otra parte

--el autor del primer libro
>>> print raiz[0][1].text
Milan Kundera

--fecha del primer libro
>>> print raiz[0][2].attrib
{u'a\xf1o': '1973'}

--el titulo del segundo libro
>>> print raiz[1][0].text
Pantaleón y las visitadoras

--atrib del primer libro
>>> print raiz[0][2].attrib
{u'a\xf1o': '1973'}
--solo el año
>>> print raiz[0][2].attrib[u'a\xf1o']
1973

--año y titulo del tercer libro
>>> print raiz[2][2].attrib[u'a\xf1o'], raiz[2][0].text
1969 Conversación en la catedral


--asignamos varibales
>>> libro1 = raiz[0]
>>> libro2 = raiz[1]
>>> libro3 = raiz[2]
>>> print libro1
<Element libro at 0x7fab8ed487a0>
--imprimimos el titulo del libro1
>>> print libro1[0].text
La vida está en otra parte
--el año y titulo del tercer libro con la variable del año
>>> anyo = u'a\xf1o'
>>> print raiz[2][2].attrib[anyo], raiz[2][0].text
1969 Conversación en la catedral

