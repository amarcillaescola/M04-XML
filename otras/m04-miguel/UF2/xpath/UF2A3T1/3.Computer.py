#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO1
#Esborrar els bancs 0 i 2 de memòria (hi ha 4 bancs).

from lxml import etree
arbol = etree.parse ('fitxersortida.xml')
componentes = arbol.getroot()
lista_banks_cero = componentes.xpath("//node[@id=\"bank:0\"]")
lista_banks_dos = componentes.xpath("//node[@id=\"bank:2\"]")

for bank in lista_banks_cero:
	bank.getparent().remove(bank)
	
for bank in lista_banks_dos:
	bank.getparent().remove(bank)
	
arbol.write("fitxersortida_3.xml")







