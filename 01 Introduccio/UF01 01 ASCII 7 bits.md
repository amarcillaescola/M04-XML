ASCII emplea grupos de **7 bits para codificar caracteres en binario,** permitiendo representar a 27 = 128 caracteres. 
Los caracteres ASCII son caracteres cuyos puntos de código van desde 0x00 a 0x7F. Entonces, para codificar cualquier carácter ASCII en binario, necesitamos al menos 7 bits. **En la práctica, se agrega un octavo bit y se usa como bit de paridad para detectar errores de transmisión.** Su tabla de correspondencias es la siguiente:

[Tabla ASCII de 7 bits](https://www.ionos.es/digitalguide/servidores/know-how/ascii-american-standard-code-for-information-interchange/)

01000010 01001001 01000101 01001110 01010110 01000101 01001110 01001001 01000100 01001111 01010011 00101111 01000001 01010011 00100000 01000001 01001100 00100000 01001101 01001111 01000100 01010101 01001100 01001111 00100000 01001101 00110000 00110110 00101101 01011000 01001101 01001100 

BIENVENIDOS/AS AL MODULO M06-XML