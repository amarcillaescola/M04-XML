### <u>Exercicis per fer amb python i XPath (poblacions.xml)</u>

__1. Llistat de totes les provincies.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
for provincia in provincias:
  print provincia.text
```

__2. Llistat de tots els municipis.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
for municipi in municipis:
  print municipi.text
```

__3. Mostra la lista de provincies i el total de municipis que te cadascuna.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
raiz = arbol.getroot()
for provincia in provincias:
	num_municipis = provincia.xpath("count(../localidades/localidad)")
	print provincia.text, num_municipis
```

__4. Demana per teclat el nom d'una província i mostra els seus municipis.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
raiz = arbol.getroot()
nom_provincia=raw_input("Dime el nombre de una provincia: ")
print nom_provincia
for provincia in provincias:
	if provincia.text == nom_provincia: # provincia.find("nombre").text == nom_provincia
		#cogemos la ruta .. porque empezamos desde provincia para buscar las localidades de la introducida
		municipis = provincia.xpath('../localidades/localidad')
		for municipi in municipis:
			print municipi.text
```
__5. Demana per teclat el nom d'un municipi i mostra la provincia on es troba.__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
#provincias = arbol.xpath ('/lista/provincia/nombre')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad') #xpath siempre te devuelve una lista vacia, un elemento o n elementos
raiz = arbol.getroot()
nom_municipi=raw_input("Dime el nombre de un municipio: ")
print nom_municipi
for municipi in municipis:
	if municipi.text == nom_municipi:
		provincias = municipi.xpath('../../nombre')
		for provincia in provincias:
			print provincia.text
```

__6. Mostra la província amb més municipis i la que té menys (província i número de municipis)__
```
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
#municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
raiz = arbol.getroot()
max=0
max_muni=''
min=10000000
min_muni=''
for provincia in provincias:
	num_municipis = provincia.xpath("count(../localidades/localidad)")
	if num_municipis > max:
		max=num_municipis
		max_muni= provincia.text

	if num_municipis < min:
		min=num_municipis
		min_muni=provincia.text
print "Provincia con mas municipios: ", max_muni, "con ", max
print "Provincia con menos municipios: ", min_muni, "con ", min
```
