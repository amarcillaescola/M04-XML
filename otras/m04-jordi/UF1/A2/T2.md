Validar documents XML
=====================

MP4UF1A2T2

Pràctica amb eines per validar documents XML respecte el seu DTD

Eines de validació
------------------

Opcions avançades de l’ordre `xmllint`(1)

-   `--format`
-   `--dtdattr`
-   `--encode`
-   `--noent`
-   `--nonet`
-   `--shell`

El catàleg
----------

-   El catàleg és com una base de dades que permet recuperar DTDs
    situats en fitxers locals usant com a claus identificador públics
    o URLs. Això evita fer descàrregues de fitxers remots.
-   Les eines `xmllint`(1) i `xsltproc`(1) usen un catàleg per
    optimitzar l’accés als DTDs.
-   El “punt d’entrada” del catàleg és `/etc/xml/catalog`.
-   L’ordre `xmlcatalog`(1) facilita la gestió del catàleg, encara que
    res impedeix editar-lo directament.

Enllaços recomanats
-------------------

La presentació XML Avanzado serà presentada, parcialment, al llarg de
diverses classes.

-   [XML
    Avanzado](http://www.di.uniovi.es/~labra/cursos/XMLAvanzado/XML.html)
    ([local](aux/XMLAvanzado/XML.html))
-   [XML Syntax Quick
    Reference](http://www.mulberrytech.com/quickref/XMLquickref.pdf)
    ([local](../A1/aux/XMLquickref.pdf))

Pràctiques
----------

-   Instal·la el paquet `xhtml1-dtds` (abans guarda una còpia de
    `/etc/xml/catalog`). Compara ara `/etc/xml/catalog` amb la còpia que
    has fet (usa les ordres `diff`(1), o `meld` per comparar fitxers).
-   Consulta el contingut del paquet (`rpm -ql xhtml1-dtds`). Què hi ha
    en els fitxers amb extensió `.ent`? Com es combinen “dins” els
    fitxers amb extensió `.dtd`?
-   Valida documents XHTML baixats d’Internet. Per exemple:

        $ wget -O x.html http://www.w3c.org/
        $ xmllint --nonet --noout --valid x.html

-   Estudia la presentació XML Avanzado mencionada en la
    llista d’enllaços.

