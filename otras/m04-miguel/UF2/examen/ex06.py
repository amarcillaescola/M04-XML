#!/usr/bin/python
# -*- coding:utf-8-*-
#Miguel Amoros
#Esborra el professor Rafel
import sys
from lxml import etree
root = etree.parse ('institut_examen.xml')

profesor_rafel = root.xpath("//Professor[Nom=\"Rafel\"]")

#Para el profe con nombre Rafel, lo borramos de su elemento padre
for professor in profesor_rafel:
	professor.getparent().remove(professor)
	
	
root.write("institut_examen.xml")

sys.exit(0)
