# <u>PRÀCTICA 2 - ARBRES BINARIS</u>
## Exemple
![Exemple](./aux/2.1.png "Exemple pràctica")  
```
<?xml version="1.0" encoding="UTF-8"?>
<bookstore>
<book category="cooking">
 <title lang="en">Everyday Italian</title>
 <author>Giada De Laurentiis</author>
 <year>2005</year>
 <price>30.00</price>
</book>
<book category="children">
 <title lang="en">Harry Potter</title>
 <author>J K. Rowling</author>
 <year>2005</year>
 <price>29.99</price>
</book>
<book category="web">
 <title lang="en">Learning XML</title>
 <author>Erik T. Ray</author>
 <year>2003</year>
 <price>39.95</price>
</book>
</bookstore>
```
##### 2.- Representar l'estructura d'arbre i escriure un document XML que representi la següent informació sobre la carta del menú d'esmorzars d'un restaurant:
![Exercici2](./aux/2.2.a.png)  

<u>__Solució:__</u>  
![arbre2](./aux/2.2.png)

<u>__Script:__</u>  
__[Enllaç script exercici 2](./scripts/2.2.xml)__

##### 3.- Representar l'estructura d'arbre i escriure un document XML que representi la següent informació:
![Exercici3](./aux/2.3.a.png)  

<u>__Solució:__</u>  
![arbre3](./aux/2.3.png)

<u>__Script:__</u>  
__[Enllaç script exercici 3](./scripts/2.3.xml)__

##### 4.- Escriure un document XML per guardar la següent informació sobre arbres:
```
Acer monspessulanum

Nombre común: Arce de Montpellier, Arce menor
Vegetación: Caducifolio
Altura: De 6 a 10 metros
Forma y estructura: Copa esférica. Tronco principal recto con bifurcaciones. Ramaje colgante
Color en primavera: Haz verde brillante, envés verde blanquecino
Resistencia a las heladas: Heladas fuertes (hasta -15ºC)
Otros: Las hojas se utilizan como infusión
```
```
Olea europea

Nombre común: Olivo
Vegetación: Perenne
Altura: De 8 a 15 metros
Forma y estructura: Copa irregular. Tronco principal irregular con bifurcaciones.cRamaje tortuoso
Color en primavera: Haz verde oscuro, envés verde plateado
Resistencia a las heladas: Heladas medias (hasta -10ºC)
```
```
Platanus orientalis

Nombre común: Platano
Vegetación: Caducifolio
Altura: De 20 a 25 metros
Forma y estructura: Copa ovoidal. Tronco principal recto. Ramaje expandido
Color en primavera: Haz verde medio, enves verde claro
Color en otoño: Ocre
Resistencia a las heladas: Heladas fuertes (hasta -20ºC)
```
```
Quercus ilex

Nombre común: Encina
Vegetación: Perenne
Altura: En torno a 25 metros
Forma y estructura: Copa esférica o elíptica irregular. Tronco principal recto. Ramaje tortuoso
Resistencia a las heladas: Heladas fuertes (hasta -15ºC)
```
<u>__Script:__</u>  
__[Enllaç script exercici 4](./scripts/2.4.xml)__

##### 5.- Escriure un document XML per guardar la següent factura d'una empresa de productes informàtics:
![Exercici5](./aux/2.5.png)   
<u>__Script:__</u>  
__[Enllaç script exercici 5](./scripts/2.5.xml)__
