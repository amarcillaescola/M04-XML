# UF2. Portfoli
## git, markdown, pandoc
---

<ins>S'avaluarà tant la `qualitat` del **contingut** com de la **presentació**.</ins>


Es demana:

Crear un _directori_ `git`, on a dintre hi tindreu els següents fitxers.

## Fitxers/directoris que ha d'incloure:

* README.md
* Directori aux
* Directori slides
  - git.md 
  - git.pdf
  - git.html  
# Fitxer `README.md`

<ins>Contingut</ins>:

Es demana l'explicació de git, segons l'index que hi ha a continuació. A banda de posar quina és 
la sintaxi de cada ordre per git, caldrà il·lustrar l'explicació amb exemples propis.

- Organització les zones de treball i conceptes bàsics
- Creació inicial
- Sincronització local/remot
- Fitxers .gitignore i .gitkeep
- Ús de claus
- Operacions amb el remot (add, delete)
- Creació de branques, canvi entre branques, fusions i esborrats
- Desfer canvis, en els casos més habituals:
    - Fitxers no seguits
    - Desfer canvis afegits a l'index
    - Desfer un commit
- Qualsevol punt que considereu interessant

# Directori `aux`

Tot allò necessari per implementar el que es demana

# Directori `slides`

Contindrà els fitxers slides generats amb pandoc amb els formats slidy i beamer

# Fitxer git.md 

Serà l'adequació del fitxer README.md per tenir un contingut de presentació, és a dir,
resumit i amb poc de text per cadascún dels punts.

# Fitxers git.html i git.pdf

Fitxers de diapositives de presentació en format html i pdf respectivament, resultat de processar git.md amb pandoc.

**`IMPORTANT`**
**Es comprovarà l'ús del contingut per part de l'alumne així com l'assoliment del contingut de la presentació**
