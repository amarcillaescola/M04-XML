#!/usr/bin/python3
#Miguel Amorós
# Formulari amb selector múltiple per comunitats autònomes espanyoles:
# cal respondre amb el nombre d’habitants de cada comunitat i la suma total dels mateixos (usa aquests
# codis per identificar les comunitats en el formulari).

import sys
import cgi

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

# conseguimos los valores del selector name="operacion" del form
c_autonomas = form.getvalue("comunidades")
dicc_comunidades={}

if "Andalucia" in c_autonomas:
    dicc_comunidades['ES-AN']={'Comunidad':'Andalucia', 'Habitantes': 8405294 }
if "Aragon" in c_autonomas:
    dicc_comunidades['ES-AR']={'Comunidad':'Aragon', 'Habitantes': 1308563 }
if "Canarias" in c_autonomas:
    dicc_comunidades['ES-CN']={'Comunidad':'Canarias', 'Habitantes': 2127685 }
if "Cantabria" in c_autonomas:
    dicc_comunidades['ES-CB']={'Comunidad':'Cantabria', 'Habitantes': 582206 }
if "Mancha" in c_autonomas:
    dicc_comunidades['ES-CM']={'Comunidad':'Castilla-La Mancha', 'Habitantes': 2041631 }
if "Leon" in c_autonomas:
    dicc_comunidades['ES-CL']={'Comunidad':'Castilla y León', 'Habitantes': 2410819 }
if "Catalunya"in c_autonomas:
    dicc_comunidades['ES-CT']={'Comunidad':'Catalunya', 'Habitantes': 7516544 }
if "Ceuta" in c_autonomas:
    dicc_comunidades['ES-CE']={'Comunidad':'Ceuta', 'Habitantes': 84519 }
if "Madrid" in c_autonomas:
    dicc_comunidades['ES-MD']={'Comunidad':'Comunidad de Madrid', 'Habitantes': 6587711 }
if "Valenciana" in c_autonomas:
    dicc_comunidades['ES-VC']={'Comunidad':'Comunidad Valenciana', 'Habitantes': 4948411 }
if "Extremadura" in c_autonomas:
    dicc_comunidades['ES-EX']={'Comunidad':'Extremadura', 'Habitantes': 1087778 }
if "Galicia" in c_autonomas:
    dicc_comunidades['ES-GA']={'Comunidad':'Galicia', 'Habitantes': 2699299 }
if "Balears" in c_autonomas:
    dicc_comunidades['ES-IB']={'Comunidad':'Illes Balears', 'Habitantes': 1107220 }
if "Melilla" in c_autonomas:
    dicc_comunidades['ES-ME']={'Comunidad':'Melilla', 'Habitantes': 86026 }
if "Rioja" in c_autonomas:
    dicc_comunidades['ES-RI']={'Comunidad':'La Rioja', 'Habitantes': 315794 }
if "Asturias" in c_autonomas:
    dicc_comunidades['ES-AS']={'Comunidad':'Principado de Asturias', 'Habitantes': 1042608 }
if "Murcia" in c_autonomas:
    dicc_comunidades['ES-MC']={'Comunidad':'Región de Murcia', 'Habitantes': 1464847 }
if "Navarra" in c_autonomas:
    dicc_comunidades['ES-NC']={'Comunidad':'Navarra', 'Habitantes': 640647 }
if "Vasco" in c_autonomas:
    dicc_comunidades['ES-PV']={'Comunidad':'País Vasco', 'Habitantes': 2172591 }

# Mostramos los resultados:
total_habitantes=0

for k in dicc_comunidades:
    total_habitantes= total_habitantes + float(dicc_comunidades[k]['Habitantes'])
    print(k, dicc_comunidades[k])
    print("<br>")
print("<br>")
print('El número total de habitantes es: %d' % total_habitantes)
print('')
#redirectURL = "http://localhost:8000/UF3A1T3_calculadora.html"
print('<html>')
print('  <head><br><br>')
print('    <a href="http://localhost:8000/UF3A1T3_comunidades.html">Volver atrás</a>')
print('  </head>')
print('</html>')

sys.exit(0)

# vim:sw=4:ts=4:ai:et
# >>> dic={1:{'nombre': 'miguel', 'apellidos': 'AmorosMoret'}}
# >>> dic
# {1: {'nombre': 'miguel', 'apellidos': 'AmorosMoret'}}
# >>> dic[2]={'nombre': 'Pere', 'apellidos': 'canet'}
# >>> dic
# {1: {'nombre': 'miguel', 'apellidos': 'AmorosMoret'}, 2: {'nombre': 'Pere', 'apellidos': 'canet'}}
# >>>
# >>> dic[1]
# {'nombre': 'miguel', 'apellidos': 'AmorosMoret'}
# >>> dic[1]['nombre']
# 'miguel'
