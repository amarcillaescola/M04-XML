# ESQUEMES

__Aquí teniu un fitxer XML i el corresponent esquema que el valida. Per validar-lo tan sols heu d'executar les següents opcions de xmllint:__

`xmllint  5.0.receptes.xml --schema 5.0.receptes.xsd --noout`  

__Si tot és correcte, l'anterior ordre retornarà el següent missatge:__

`5.0.receptes.xml validates`

## Exercicis:

__5.1. Tenim el següent fitxer XML [5.1.libraryAmbErrors.xml](./scripts/5.1.libraryAmbErrors.xml)__

__<u>Apartat A</u>. Reviseu i corregiu errors. El nou fitxer es dirà 5.1.library.xml.__

```
<? xml version = "1.0"?>
<! DOCTYPE library
[
      <! ELEMENT library (book +)>
      <! ELEMENT book (títol, autor, ref?, genre+)>
      <! ELEMENT title (#PCDATA)>
      <! ELEMENT author (#PCDATA)>
      <! ELEMENT ref (#PCDATA)>
      <! ELEMENT genre (drama|comedia|accio|infantil)>
 ]
>
<Library>
<Llibre>
        <title> N o M </ title>
        <autor> Agatha Christie </ author>
        <Ref> detectiu-C-15 </ ref>
</ Book>
<Llibre>
        <title> El gos de Baskerville </ title>
        <autor> Sir Arthur Conan Doyle </ author>
        <Ref> detectiu-D-3 </ ref>
</ Book>
<Llibre>
        <Títol> Duna </ title>
        <autor> Franck Heckbert </ author>
        <Ref> ficció-H-1 </ ref>
</ Book>
</ Biblioteca>
```

[script amd DTD i XML arreglat](./scripts/5.1.libraryDTD.xml)  

__<u>Apartat B.</u> Declarar un esquema vàlid (5.1.library.xsd) pel fitxer 5.1.library.xml__

[script XML](./scripts/5.1.library.xml)  
[script XSD](./scripts/5.1.library.xsd)  

__Validació:__  

`xmllint 5.1.library.xml --schema 5.1.library.xsd --noout`

```
[isx46410800@miguel-fedora27 scripts]$ xmllint 5.1.library.xml --schema 5.1.library.xsd --noout
5.1.library.xml validates
```

__5.2. Feu una còpia del fitxer `5.0.receptes.xml` amb el nom `5.0.receptesAmbErrors.xml`. Introduiu errors expressament ( per exemple canviar valors, on toca booleà posar altre tipus de dades o en la llista enumerada posar un valor no vàlid, etc). Comproveu amb xmllint els missatges d'error que dóna i explique-los al fitxer `5.2.receptes.md`.__

[fitxer 5.2.receptes.md](./5.2.receptes.md)  

__5.3. Donat el fitxer `5.3.coleccio.xml`, crea el seu esquema corresponent 5.3.coleccio.xsd.__  

[script XML](./scripts/5.3.coleccio.xml)  
[script XSD](./scripts/5.3.coleccio.xsd)  

```
[isx46410800@miguel-fedora27 scripts]$ xmllint 5.3.coleccio.xml --schema 5.3.coleccio.xsd --noout
5.3.coleccio.xml validates
```

__5.4. Donat aquest fitxer `5.4.llibres.xml`, crea el seu esquema corresponent `5.4.llibres.xsd`.__  

[script XML](./scripts/5.4.llibres.xml)  
[script XSD](./scripts/5.4.llibres.xsd)  

```
[isx46410800@miguel-fedora27 scripts]$ xmllint 5.4.llibres.xml --schema 5.4.llibres.xsd --noout
5.4.llibres.xml validates
```
