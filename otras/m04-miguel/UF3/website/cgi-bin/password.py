#!/usr/bin/python3
#Miguel Amorós
# Formulari per canvi de contrasenya: demanar dues vegades nova
# contrasenya i verificar que són iguals; en cas d’error actuar com en
# el cas anterior.

import sys
import cgi

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

# conseguimos los valores del login y password
password_old = form.getvalue("passActual")
password_new1 = form.getvalue("passNew1")
password_new2 = form.getvalue("passNew2")

if password_old != password_new1 and password_new1==password_new2:
    print("Contraseña cambiada correctamente")
    print("<br>")
    print('')
    print('<html>')
    print('  <head><br><br>')
    print('    <a href="http://localhost:8000/UF3A1T3_password.html">Volver atrás</a>')
    print('  </head>')
    print('</html>')
else:
    print("No se puede cambiar la contraseña")
    print("<br>")
    redirectURL = "http://localhost:8000/UF3A1T3_password.html"
    print('<html>')
    print('  <head>')
    print('    <meta http-equiv="refresh" content="0;url='+str(redirectURL)+'" />')
    print('  </head>')
    print('</html>')

sys.exit(0)

# vim:sw=4:ts=4:ai:et
