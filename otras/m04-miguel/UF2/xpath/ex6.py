#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO6
#6.Mostra la província amb més municipis i la que té menys (província i número de municipis)

from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
#municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
raiz = arbol.getroot()
max=0
max_muni=''
min=10000000
min_muni=''

for provincia in provincias:
	num_municipis = provincia.xpath("count(../localidades/localidad)")
	if num_municipis > max:
		max=num_municipis
		max_muni= provincia.text
		
	if num_municipis < min:
		min=num_municipis
		min_muni=provincia.text
		
print "Provincia con mas municipios: ", max_muni, "con ", max
print "Provincia con menos municipios: ", min_muni, "con ", min

