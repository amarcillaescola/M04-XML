#!/usr/bin/python
# -*- coding:utf-8-*-
#Miguel Amoros
#El modul m04 passa a tenir 8h de docencia
import sys
from lxml import etree
root = etree.parse ('institut.xml')

lista_modul_m04 = root.xpath("//Horari[@modul = \"m04\"]")

#Para el modulo de m04, le modificamos su duracion a 4
for modul in lista_modul_m04:
	modul.attrib["durada"]=str(4)


root.write("institut_examen.xml")

sys.exit(0)
