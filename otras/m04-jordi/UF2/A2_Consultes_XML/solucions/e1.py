#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Exercici 1. Llistat de totes les provincies.

from lxml import etree
arbol = etree.parse ("poblacions.xml")
provincias = arbol.xpath ("/lista/provincia/nombre")
for provincia in provincias :
    print provincia.text
