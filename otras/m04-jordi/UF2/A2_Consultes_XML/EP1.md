Interrogació de documents XML
=============================

MP4UF2A2EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Exercici pràctic.

### Tipus d’exercici

Aquest exercici servirà per mesurar els coneixements de mòdul
`ElementTree` de Python i les técniques d’extracció de dades de SGBD
relacionals en format XML.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 50% de la nota del resultat d’aprenentatge associat
a l’activitat.
