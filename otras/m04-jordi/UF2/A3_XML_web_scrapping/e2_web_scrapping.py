#!/usr/bin/python3

from lxml import html
import requests

# https://docs.python-guide.org/scenarios/scrape/

page = requests.get('http://econpy.pythonanywhere.com/ex/001.html')

# We need to use page.content rather than page.text because html.fromstring
# implicitly expects bytes as input.
tree = html.fromstring(page.content)

# Fetching information of buyers
llista_compradors_preus  = tree.xpath('//div[@title="buyer-info"]')

# Buyer (position 0): //div[@title="buyer-name"]
# Price (position 1): //span[@class="item-price"]

#This will shows  a list of buyers and prices 
for buyer in llista_compradors_preus:
    print ("Buyer is {} and price is {}".format(buyer[0].text, buyer[1].text))
