#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO5
#5. Demana per teclat el nom d'un municipi i mostra la provincia on es troba.
from lxml import etree
arbol = etree.parse ('poblacions.xml')
#provincias = arbol.xpath ('/lista/provincia/nombre')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad') #xpath siempre te devuelve una lista vacia, un elemento o n elementos
raiz = arbol.getroot()

nom_municipi=raw_input("Dime el nombre de un municipio: ")
print nom_municipi

#para cada municipio, que sea igual al introducido
for municipi in municipis:
	if municipi.text == nom_municipi:
		#cogemos la ruta para atras hasta coger los nombres de provincia
		provincias = municipi.xpath('../../nombre')
		#al ser una lista lo que retorna xpath, iteramos con un for
		for provincia in provincias:
			print provincia.text
		#seria lo mismo ya que es una lista y es de solo un elemento
		#print provincias[0].text
