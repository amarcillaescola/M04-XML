#!/usr/bin/python
# -*- coding:utf-8-*-
#Miguel Amoros
#Mostreu el nom dels moduls que a les 9 s'estan impartint
import sys
from lxml import etree
root = etree.parse ('institut_examen.xml')

lista_horas_modulos = root.xpath("//Classe/@hora[.<=9]/../../@modul")

#Para cada modulo que empiece antes de las 9(tienen durada minimo de 2)
#printamos el modulo
for modulo in lista_horas_modulos:
	print modulo

root.write("institut_examen.xml")

sys.exit(0)
