#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# 5. Demana per teclat el nom d'un municipi i mostra la provincia on es troba.

from lxml import etree
arbol = etree . parse ("poblacions.xml")
localidad = raw_input ("\nIntrodueix una localitat i et diré a quina província pertany: ")

print "\n\n Opció 1"
provincias = arbol.xpath(u"//localidad[.=\"%s\"]/../../nombre"  % localidad )
for provincia in provincias:
    print " %s es troba a %s" % (localidad , provincia.text )

print "\n\n Opció 2"
localidades = arbol.xpath(u"//localidad[.=\"%s\"]"  % localidad )
for loc in localidades:
    provincia = loc.getparent().getparent().find("nombre").text
    print " %s es troba a %s" % (localidad, provincia)
