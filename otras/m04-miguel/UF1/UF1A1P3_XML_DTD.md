# <u>PRÀCTICA 3</u>  
#### Corregir errors en el XML (no en el DTD!) dels següents documents:

__3.1 Números__  
+ Eliminar el subelement 'numero' per a que segueixi el DTD indicat:  
`<numeros>25</numeros>`

__3.2 Letras__
+ Eliminar una repetició de subelement 'letra' per a què segueixi el DTD indicat:
```
<letras>
  <letra>m</letra>
</letras>
```

__3.3. Colores__  
+ Posar correctament les etiquetes del element 'negro':
```
<colores>
  <color>azul marino</color>
  <color>negro</color>
  <color>amarillo</color>
</colores>
```

__3.4. Flores__
+ Posar com a mínim una vegada l'element 'flor' per a què segueixi el DTD indicat:
```
<flores>
  <flor>Amapola</flor>
</flores>
```

__3.5. Animales__
+ Posar els subelements 'perro' i 'gato' como a 'animal' per a què segueixi el DTD indicat:
```
<animales>
  <animal>Perro Caniche</animal>
  <animal>Gato Siamés</animal>
</animales>
```

__3.6. Escritores__
+ Posar el ordre correcte, primer 'nombre' i després 'nacimiento' per a què segueixi el DTD indicat:
```
<escritores>
  <escritor>
    <nombre>Mario Vargas LLosa</nombre>
    <nacimiento>28 de marzo de 1936</nacimiento>
  </escritor>
  <escritor>
    <nombre>Milan Kundera</nombre>
    <nacimiento>1 de abril de 1929</nacimiento>
  </escritor>
</escritores>
```

__3.7. Músicos__
+ Posar només el subelement 'nombre' o 'apodo', ja que s'ha de posar un o l'altre per a què segueixi el DTD indicat:
```
<musicos>
  <musico>
    <nombre>Antonio Vivaldi</nombre>
    <fechaNacimiento>4 de marzo de 1678</fechaNacimiento>
  </musico>
  <musico>
    <apodo>El viejo peluca</apodo>
    <fechaNacimiento>21 de marzo de 1685</fechaNacimiento>
  </musico>
</musicos>
```

__3.8. Teléfonos de emergencia__
+ En el primer 'contacto' s'ha de posar com a mínim un 'telefonoFijo' i en el segon, s'ha de posar en el ordre correcte, primers els 'telefonoFijo' per a què segueixi el DTD indicat:
```
<agenda>
  <contacto>
    <nombre>Ayuntamiento</nombre>
    <telefonoFijo>010</telefonoFijo>
    <telefonoMovil>Desconocido</telefonoMovil>
  </contacto>
  <contacto>
    <nombre>Emergencias</nombre>
    <telefonoFijo>112 (Unión Europea)</telefonoFijo>
    <telefonoFijo>911 (Estados Unidos)</telefonoFijo>
    <telefonoMovil>Desconocido</telefonoMovil>
  </contacto>
</agenda>
```

__3.9. El sistema solar__
+ Només es pot posar un subelement entre planeta, satelite o asteroide i el segon element no té correcte les etiquetes per a què segueixi el DTD indicat:
```
<sistemaSolar>
  <cuerpo>
    <planeta>Tierra</planeta>
  </cuerpo>
  <cuerpo>
    <asteroide>Ceres</asteroide>
  </cuerpo>
</sistemaSolar>
```

#### Corregir errors en el DTD intern:  
__3.10. Marcadores__
+ Posar un '\*' en el element 'marcador' per indicar que es por repetir:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE marcadores [
    <!ELEMENT marcadores (marcador*)>
    <!ELEMENT marcador (nombre, uri)>
    <!ELEMENT nombre (#PCDATA)>
    <!ELEMENT uri (#PCDATA)>
]>
```

__3.11. Efemérides__
+ Posar un '\*' en el element 'efemeride' per indicar que es por repetir i posar els #PCDATA de fecha i hecho:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE efemerides [
    <!ELEMENT efemerides (efemeride*)>
    <!ELEMENT efemeride (fecha, hecho)>
    <!ELEMENT fecha (#PCDATA)>
    <!ELEMENT hecho (#PCDATA)>
]>
```

__3.12. Aeropuertos__
+ Posar un '?' en el element 'cerrado' ja que pot ser opcional a que surti o no i posar també como EMPTY cerrado:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE aeropuertos [
    <!ELEMENT aeropuertos (aeropuerto*)>
    <!ELEMENT aeropuerto (nombre, cerrado?)>
    <!ELEMENT nombre (#PCDATA)>
    <!ELEMENT cerrado (EMPTY)>
]>
```

__3.12. Vuelos__
+ Posar l'opció de dues formes d'ordre en vuelo.
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE vuelos [
<!ELEMENT vuelos (vuelo*)>
<!ELEMENT vuelo ((origen, destino)|(destino,origen))>
<!ELEMENT origen (#PCDATA)>
<!ELEMENT destino (#PCDATA)>
]>
```
__3.13. Países__
+ Indicar que 'unionEuropea' i 'otan' amb un '?' ja que pot ser opcional i el element nombre ha de ser #PCDATA:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE paises [
    <!ELEMENT paises (pais*)>
    <!ELEMENT pais (nombre, unionEuropea?, otan?)>
    <!ELEMENT nombre (#PCDATA))>
    <!ELEMENT unionEuropea EMPTY>
    <!ELEMENT otan EMPTY>
]>
```

__3.14. Códigos de Colores__
+ Indicar que color té diferents subelements i que pot ser un d'ells només:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE colores [
    <!ELEMENT colores (color*)>
    <!ELEMENT color (nombreSvg | rgb | cmyk)>
    <!ELEMENT nombreSvg (#PCDATA)>
    <!ELEMENT rgb (#PCDATA)>
    <!ELEMENT cmyk (#PCDATA)>
]>
```

__3.15. Contabilidad__
+ Indicar que es pot repetir 'apunte', que dins de apunte pot estar 'ingreso' o 'gasto', fecha, cantidad, concepto, i los EMPTY:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE contabilidad [
    <!ELEMENT contabilidad (apunte*)>
    <!ELEMENT apunte ((ingreso | gasto),fecha, cantidad, concepto)>
    <!ELEMENT ingreso EMPTY>
    <!ELEMENT gasto EMPTY>
    <!ELEMENT fecha (#PCDATA)>
    <!ELEMENT cantidad (#PCDATA)>
    <!ELEMENT concepto (#PCDATA)>
]>
```

__3.16. Mensajes__
+ Indicar que 'mensaje' es pot repetir amb un * i que té diferents subelements. El subelement texto pot tenir 'strong', que pot ser opcional:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mensajes [
    <!ELEMENT mensajes (mensaje*)>
    <!ELEMENT mensaje (de, para, hora, texto)>
    <!ELEMENT de (#PCDATA)>
    <!ELEMENT para (#PCDATA)>
    <!ELEMENT hora (#PCDATA)>
    <!ELEMENT texto (#PCDATA | strong)*>
    <!ELEMENT strong (#PCDATA)>
]>
```

#### Corregir els errors que hi hagi al xml  
__3.15. Datos personales__  
`<persona nombre="03141592E" />`
> Canviar dni per nombre.

__3.16. Película__
`<pelicula titulo="La diligencia" />`
> Treure genero i deixar només titulo.

__3.17. Cuadros__
```
<cuadros>
  <cuadro titulo="Adán y Eva" autor="Alberto Durero" />
  <cuadro titulo="Adán y Eva 2" autor="Lucas Cranach, el viejo" />
</cuadros>
```
> Posar un titulo diferent perquè és un camp ID i posar en ordre correcte els atributs.

__3.18. Lista de compra__
```
<listaCompra>
  <item>
    <leche nombre="Pascual" cantidad="12 litros" ></leche>
    <pan nombre="Puleva" cantidad="3 barras de cuarto" />
  </item>
</listaCompra>
```
> Posar l'element item i afegir i posar en ordre l'atribut nombre.

__3.19. Jugadores de fútbol__
```
<futbol>
  <jugador nombre="Alfredo Di Stéfano" codigo="A1" />
  <jugador nombre="Pelé Edson Arantes do Nascimento" codigo="A2" />
  <jugador nombre="Diego Armando Maradona" codigo="A3" />
  <jugador nombre="Johan Cruyff" codigo="A4" />
</futbol>
```
> Les paraules de 'nombre' han d'anar separades per comes i els codis no han de començar per dígit.

__3.20. Jugadores y equipos de futbol__
```
<futbol>
  <jugador nombre="Alfredo Di Stéfano" codigo="ads" />
  <jugador nombre="Edison Arantes do Nascimento" codigo="ean" />
  <jugador nombre="Diego Armando Maradona" codigo="dam" />
  <jugador nombre="Johan Cruyff" codigo="jc" />
  <equipo nombre="Società Sportiva Calcio Napoli" jugadores="dam" />
  <equipo nombre="Futbol Club Barcelona" jugadores="ean" />
  <equipo nombre="Futbol Club Barcelona" jugadores="dam jc" />
</futbol>
```
> Jugadores han de seguir els ID de codigo i només un valor per atribut, es pot posar varis separades per espai.

#### Corregir els errors que hi hagi al DTD intern.
__3.21. Libro__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE libro [
  <!ELEMENT libro EMPTY>
  <!ATTLIST libro autor NMTOKENS #REQUIRED>
]>
```
> Canviar NMTOKEN per NMTOKENS

__3.22. Inventores__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE inventores [
  <!ELEMENT inventores (inventor*)>
  <!ELEMENT inventor EMPTY>
  <!ATTLIST inventor nombre CDATA #IMPLIED>
  <!ATTLIST inventor invento CDATA #REQUIRED>
]>
```
> Indicar el element inventor i posar CDATA i #IMPLIED a atribut nombre.

__3.23. Cosas por hacer__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE cosasPorHacer [
  <!ELEMENT cosasPorHacer (cosa*)>
  <!ELEMENT cosa #PCDATA>
  <!ATTLIST cosa fecha CDATA #REQUIRED>
  <!ATTLIST cosa asunto CDATA #IMPLIED>
  <!ATTLIST cosa fechaLimite CDATA #REQUIRED>
]>
```
> Posar cosa com #PCDATA i asunto com a atribut opcional.

__3.24. Resoluciones de pantalla__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resoluciones [
  <!ELEMENT resoluciones (resolucion*)>
  <!ELEMENT resolucion EMPTY>
  <!ATTLIST resolucion nombre CDATA #REQUIRED>
  <!ATTLIST resolucion alto CDATA #REQUIRED>
  <!ATTLIST resolucion ancho CDATA #REQUIRED>
]>
```
> Posar el element resolucion com a EMPTY i atribus CDATA #REQUIRED

__3.25. Álbumes de Mortadelo y Filemón__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE albumesMortadelo [
  <!ELEMENT albumesMortadelo (album*)>
  <!ELEMENT album EMPTY>
  <!ATTLIST album nombre CDATA #REQUIRED>
  <!ATTLIST album fecha(1969,1970,1971,1972,1973,1974) #REQUIRED>
]>
```
> Posar con a EMPTY el element album.
